#include <iostream>
#include <ctime>
#include <stdint.h>

int main()
{
  std::cout << "char        : " << sizeof(char)         << std::endl;
  std::cout << "short       : " << sizeof(short)        << std::endl;
  std::cout << "int         : " << sizeof(int)          << std::endl;
  std::cout << "long        : " << sizeof(long)         << std::endl;
  std::cout << "time_t      : " << sizeof(time_t)       << std::endl;
  std::cout << "uint8_t     : " << sizeof(uint8_t)      << std::endl;
  std::cout << "uint16_t    : " << sizeof(uint16_t)     << std::endl;
  std::cout << "uint32_t    : " << sizeof(uint32_t)     << std::endl;
  std::cout << "uint64_t    : " << sizeof(uint64_t)     << std::endl;
  std::cout << "double      : " << sizeof(double)       << std::endl;
  std::cout << "long double : " << sizeof(long double)  << std::endl;
  return 0;
}

